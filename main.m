addpath('data')
addpath('functions')
load('data/timeseries_1968_1982.mat');
load('data/geographical_characteristics.mat');

% Model input of the GR4J is the rainfall depth and the potential
% evapotranspiration
input.precipitation = timeseries.precipitation;
input.evapotranspiration = timeseries.potential_evapotranspiration;

%       X1              X2                  X3            X4      
% Min.   :  126     Min.    :-54.5   Min.    :  8   Min.   :1.21  
% 1st Qu.:  208     1st Qu. : -2.0   1st Qu. : 35   1st Qu.:1.75  
% Median :  291     Median  : -1.1   Median  : 76   Median :2.10  
% Mean   :  471     Mean    : -3.4   Mean    : 90   Mean   :2.09  
% 3rd Qu.:  359     3rd Qu. : -0.6   3rd Qu. :106   3rd Qu.:2.45  
% Max.   : 4006     Max.    :  0.8   Max.    :318   Max.   :3.47
%
% Lesse  : 205.2          -1.1760        64.80           2.4640
% Lesse_2: 209.1398       -1.1628        64.0224         2.3394
% Semois : 200            -1.1           95              3.25
% Ourthe : 175            -1.2           70              2.5
% Ambleve: 220.5          -0.52          104.5           2
% Vesdre : 285            -1.2           63              1.815
%
% x_1 = 100:250:4000;
% x_2 = -55:5:5;
% x_3 = 0:50:350;
% x_4 = 1:0.5:4;

% Initialise the parameter values that the model will be varied.
x(1) = 209.1398;
x(2) = -1.1628;
x(3) = 64.0224;
x(4) = 2.3394;

sigma(1) = 20;
sigma(2) = 0.2;
sigma(3) = 4;
sigma(4) = 0.3;

% The number of times we draw a number for the Monte Carlo analysis.
number_of_draws = 10^5;
x_monte_carlo = zeros(4, number_of_draws);
parfor parameter_number = 1:4
    % The mu value for a normal distribution is the mean. In this case the
    % calibrated value for the parameter. The sigma is the standard
    % deviation. Chosen to get a good spread.
    normal_distribution = makedist('Normal', 'mu', x(parameter_number), 'sigma', sigma(parameter_number));
    random_from_uniform = rand(number_of_draws, 1);
    x_monte_carlo(parameter_number, :) = icdf(normal_distribution, random_from_uniform );
end

% To run the model, we first need to define a number of time steps to run
% the model for. In the case of this assignment, timeseries data is
% available from 1968 through to 1982. We define a start year and a end
% year between 1968 and 1982 and then determine how many timesteps we
% should take.
start_year = 1964;
end_year = 1982;
sub_basin = 'Lesse';

selected_rainfall_depth = timeseries.precipitation(timeseries.precipitation.Year >= start_year &...
    timeseries.precipitation.Year <= end_year, :);
selected_evapotranspiration = timeseries.potential_evapotranspiration(timeseries.potential_evapotranspiration.Year...
    >= start_year & timeseries.potential_evapotranspiration.Year <= end_year, :);
selected_measured_discharge = timeseries.discharge(timeseries.discharge.Year...
    >= start_year & timeseries.discharge.Year <= end_year, :);

% Select the sub-basin we want to run the model for.
base_precipitation = table2array(selected_rainfall_depth(:, sub_basin));
base_evapotranspiration = table2array(selected_evapotranspiration(:, sub_basin));
measured_discharge = table2array(selected_measured_discharge(:, sub_basin));
basin_area = table2array(geographical_characteristics(strcmp(geographical_characteristics.Basin, sub_basin), 'Size'));

warm_up.sma_store_level = 188;
warm_up.routing_store_level = 46;

parameters = struct(...
    'sma_store_max_capacity', cell(number_of_draws, 1),...
    'exchange_coefficient',  cell(number_of_draws, 1), ...
    'reference_capacity',  cell(number_of_draws, 1), ...
    'hydrograph_time_base',  cell(number_of_draws, 1) ...
);

distribution_for_transpiration = makedist('Normal', 'mu', 0, 'sigma', 0.05);

outflow_length = length(measured_discharge);
new_evapotranspiration = zeros(outflow_length, number_of_draws);
parameter_index = 1;

parfor draw_index = 1:number_of_draws
    
    parameters(draw_index, 1) = buildParameters( ...
        x_monte_carlo(1, draw_index), ...
        x_monte_carlo(2, draw_index), ...
        x_monte_carlo(3, draw_index), ...
        x_monte_carlo(4, draw_index) ...
    );
    parameter_index = parameter_index + 1;
    
    
    random_from_uniform = rand(outflow_length, 1);
    offset_values = icdf(distribution_for_transpiration , random_from_uniform);
    
    evapotranspiration_with_offset = base_evapotranspiration .* (offset_values + 1);
    evapotranspiration_with_offset(evapotranspiration_with_offset < 0) = 0;
    
    new_evapotranspiration(:, draw_index) = evapotranspiration_with_offset;
end

relative_volume_error = zeros(number_of_draws, 1);
kling_gupta = zeros(number_of_draws, 1);
nash_sutcliffe = zeros(number_of_draws, 1);

outflow_length = length(measured_discharge);
    % 'routing_store_level',  zeros(outflow_length, 1), ...
    % 'sma_store_level',  zeros(outflow_length, 1),...
    % 'hydrograph_1_series',  zeros(outflow_length, 1),...
    % 'hydrograph_2_series', zeros(outflow_length, 1),...
results = struct(...
    'total_outflow', zeros(1, outflow_length), ...
    'joker', cell(number_of_draws, 1) ...
);
results = rmfield(results, 'joker');

parfor draw_index = 1:number_of_draws
    results_ = runGr4jModel(base_precipitation, new_evapotranspiration(:, draw_index), parameters(draw_index), warm_up);
    % Timesteps are in days, outflow in mm. Recalculate this to
    % m^3/s
    results(draw_index).total_outflow = results_.total_outflow ./ 1000 ./ 86400 .* basin_area .* 10^6;
    relative_volume_error(draw_index) = relativeVolumeError(results(draw_index).total_outflow', measured_discharge);
    kling_gupta(draw_index) = klingGupta(results(draw_index).total_outflow', measured_discharge);
    nash_sutcliffe(draw_index) = nashSutcliffe(results(draw_index).total_outflow', measured_discharge);
end

[m, ] = min(abs(relative_volume_error), [], 'all', 'linear')
[m, ] = max(kling_gupta, [], 'all', 'linear')
[m, ] = max(nash_sutcliffe, [], 'all', 'linear')


function [parameters] = buildParameters(x_1, x_2, x_3, x_4)
    % Maximum capacity of the production store, x_1 in literature.
    % Can only be a positive number
    parameters.sma_store_max_capacity = x_1;
    % Water exchange coefficient for the routing store, x_2 in literature
    parameters.exchange_coefficient = x_2;
    % Routing store maximum capacity, x_3 in literature
    % Can only be a positive number
    parameters.reference_capacity = x_3;
    % Hydrograph time parameter, named x_4 in literature.
    % Can only be greater than 0.5
    parameters.hydrograph_time_base = x_4;
end