year(1968, :) = [1 366];
year(1969, :) = [367 731];
year(1970, :) = [732 1097];
year(1971, :) = [1098 1461];
year(1972, :) = [1462 1828];
year(1973, :) = [1829 2192];
year(1974, :) = [2193 2557];
year(1975, :) = [2558 2922];
year(1976, :) = [2923 3288];
year(1977, :) = [3289 3653];
year(1978, :) = [3654 4018];
year(1979, :) = [4019 4384];
year(1980, :) = [4385 4749];
year(1981, :) = [4750 5114];
year(1982, :) = [5115 5479];
 
% plot_outflow = zeros(100000, 5479);
% for index = 1:100000
%     plot_outflow(index, :) = results(index).total_outflow';
% end

% selected_measured_discharge = timeseries.discharge(timeseries.discharge.Year...
%     >= start_year & timeseries.discharge.Year <= end_year, :);
% measured_discharge = table2array(selected_measured_discharge(:, sub_basin));
 
results_.total_outflow

yearly_means = [];
yearly_averages = zeros(1, 15);
for year_index = 1968:1982
    start_and_end_day = year(year_index, :);
    max_per_year = max(results_.total_outflow(start_and_end_day(1): start_and_end_day(2)));
    yearly_max = mean(max_per_year);
    disp(year_index)
    disp(yearly_max)
    %disp(prctile(plot_outflow(:, start_and_end_day(1): start_and_end_day(2)), 10, 'all'))
    yearly_means(end + 1) = yearly_max;
end

disp('total mean')
disp(mean(yearly_means));