addpath('data')
addpath('functions')
addpath('classes')
addpath('results')

% To run the model, we first need to define a number of time steps to run
% the model for. In the case of this assignment, timeseries data is
% available from 1968 through to 1982. We define a start year and a end
% year between 1968 and 1982 and then determine how many timesteps we
% should take.
use_validation_set = true;
if use_validation_set
    start_year = 1984;
    end_year = 1998;
    load('data/timeseries_1984_1998.mat');
else
    start_year = 1968;
    end_year = 1982;
    load('data/timeseries_1968_1982.mat');
end

sub_basin = 'Vesdre';
load('data/geographical_characteristics.mat');

% Model input of the GR4J is the rainfall depth and the potential
% evapotranspiration
input.precipitation = timeseries.precipitation;
input.evapotranspiration = timeseries.potential_evapotranspiration;

%       X1              X2                  X3            X4      
% Min.   :  126     Min.    :-54.5   Min.    :  8   Min.   :1.21  
% 1st Qu.:  208     1st Qu. : -2.0   1st Qu. : 35   1st Qu.:1.75  
% Median :  291     Median  : -1.1   Median  : 76   Median :2.10  
% Mean   :  471     Mean    : -3.4   Mean    : 90   Mean   :2.09  
% 3rd Qu.:  359     3rd Qu. : -0.6   3rd Qu. :106   3rd Qu.:2.45  
% Max.   : 4006     Max.    :  0.8   Max.    :318   Max.   :3.47
%
% Lesse  : 205.2          -1.1760        64.80           2.4640
% Lesse_2: 209.1398       -1.1628        64.0224         2.3394
% Semois : 200            -1.1           95              3.25
% Ourthe : 175            -1.2           70              2.5
% Ambleve: 220.5          -0.52          104.5           2
% Vesdre : 285            -1.2           63              1.815
%
%                   Lesse   Semois  Ourthe  Ambleve Vesdre
% Production store  188     180     180     175     210
% Routing store     46      65      45      65      45
% 

% Initialise the parameter values that the model will be varied.
basins = ["Lesse", "Semois", "Ourthe", "Vesdre"];
parameter_sets.Lesse   = Gr4jParameters(209.14,  -1.16,  64,     2.34   );
parameter_sets.Semois  = Gr4jParameters(200,     -1.1,   95,     3.25   );
parameter_sets.Ourthe  = Gr4jParameters(175,     -1.2,   70,     2.5    );
parameter_sets.Ambleve = Gr4jParameters(220.5,   -0.52,  104.5,  2      );
parameter_sets.Vesdre  = Gr4jParameters(285,     -1.2,   63,     1.815  );
parameters = parameter_sets.(sub_basin);

%                   Lesse   Semois  Ourthe  Ambleve Vesdre
% Production store  188     180     180     175     210
% Routing store     46      65      45      65      45

warm_up_sets.Lesse  = Gr4jWarmup(188, 46);
warm_up_sets.Semois = Gr4jWarmup(180, 65);
warm_up_sets.Ourthe = Gr4jWarmup(180, 45);
warm_up_sets.Ambleve = Gr4jWarmup(175, 65);
warm_up_sets.Vesdre = Gr4jWarmup(210, 45);
warm_up = warm_up_sets.(sub_basin);

selected_rainfall_depth = timeseries.precipitation(timeseries.precipitation.Year >= start_year &...
    timeseries.precipitation.Year <= end_year, :);
selected_evapotranspiration = timeseries.potential_evapotranspiration(timeseries.potential_evapotranspiration.Year...
    >= start_year & timeseries.potential_evapotranspiration.Year <= end_year, :);
selected_measured_discharge = timeseries.discharge(timeseries.discharge.Year...
    >= start_year & timeseries.discharge.Year <= end_year, :);

% Select the sub-basin we want to run the model for.
base_precipitation = table2array(selected_rainfall_depth(:, sub_basin));
base_evapotranspiration = table2array(selected_evapotranspiration(:, sub_basin));
measured_discharge = table2array(selected_measured_discharge(:, sub_basin));
basin_area = table2array(geographical_characteristics(strcmp(geographical_characteristics.Basin, sub_basin), 'Size'));

results_ = runGr4jModel(base_precipitation, base_evapotranspiration, parameters, warm_up);
% Timesteps are in days, outflow in mm. Recalculate this to
% m^3/s
results_.total_outflow = results_.total_outflow ./ 1000 ./ 86400 .* basin_area .* 10^6;
relative_volume_error = relativeVolumeError(results_.total_outflow', measured_discharge);
kling_gupta = klingGupta(results_.total_outflow', measured_discharge)
nash_sutcliffe = nashSutcliffe(results_.total_outflow', measured_discharge);

results = results_;
save(strcat('results/', 'results_', sub_basin, '_', int2str(start_year), '_', int2str(end_year)), 'results')