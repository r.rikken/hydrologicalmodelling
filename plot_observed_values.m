index_basin.Semois = 1;
index_basin.Lesse = 3;
index_basin.Ourthe = 6;
index_basin.Ambleve = 7;

basins = ["Lesse", "Semois", "Ourthe"];

data_start_date = datetime('2019-12-20', 'InputFormat', 'yyyy-MM-dd');
data_end_date = datetime('2020-01-20', 'InputFormat', 'yyyy-MM-dd');
forecast_date = datetime('2020-01-07', 'InputFormat', 'yyyy-MM-dd');

for basin = basins
    hold on
    for date_index = forecast_date:data_end_date - days(1)
        results = load(strcat('data/', datestr(date_index), '/results_internal_updater_', basin, '.mat'));
        basin_outflow_result = results.results.total_outflow;
        
        plot(date_index:date_index + days(length(basin_outflow_result) - 1), basin_outflow_result, '--')
    end
    index = 0;
    for forecast_index_date = forecast_date:data_end_date
            index = index + 1;
            forecast_index_date.Format = 'dd-MMM-yyyy';

            observed_values = load(strcat('data/', datestr(forecast_index_date), '/autres_rivieres.mat'));
            observed_value = str2double(table2array(observed_values.autres_rivieres(index_basin.(basin), 5)));
            observed_for_plot(index) = observed_value;
    end
    plot(forecast_date:data_end_date, observed_for_plot)
end