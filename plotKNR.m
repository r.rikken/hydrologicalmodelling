for index = 1:100000
    x1_x(index) = parameters(index).sma_store_max_capacity;
    x1_y(index) = kling_gupta(index);
    x2_x(index) = parameters(index).exchange_coefficient;
    x2_y(index) = kling_gupta(index);
    x3_x(index) = parameters(index).reference_capacity;
    x3_y(index) = kling_gupta(index);
    x4_x(index) = parameters(index).hydrograph_time_base;
    x4_y(index) = kling_gupta(index);
end

tiledlayout(4,1)
nexttile
scatter(x1_x, x1_y)
nexttile
scatter(x2_x, x2_y)
nexttile
scatter(x3_x, x3_y)
nexttile
scatter(x4_x, x4_y)