load('data/timeseries.mat');
load('data/geographical_characteristics.mat');

% First we need to know which subbasin we're modelling
sub_basin = 'Ambleve';

% Model input of the GR4J is the rainfall depth and the potential
% evapotranspiration
rainfall_depth = timeseries.precipitation;
evapotranspiration = timeseries.potential_evapotranspiration;

% Maximum capacity of the production store, x_1 in literature.
% Can only be a positive number
sma_store_max_capacity = 10;
% Water exchange coefficient for the routing store, x_2 in literature
exchange_coefficient = 1;
% Routing store maximum capacity, x_3 in literature
% Can only be a positive number
reference_capacity = 1;
% Hydrograph time parameter, named x_4 in literature.
% Can only be greater than 0.5
hydrograph_time_base = 3.8;

% To run the model, we first need to define a number of time steps to run
% the model for. In the case of this assignment, timeseries data is
% available from 1968 through to 1982. We define a start year and a end
% year between 1968 and 1982 and then determine how many timesteps we
% should take.
start_year = 1970;
end_year = 1980;

selected_rainfall_depth = rainfall_depth(rainfall_depth.Year >= start_year & rainfall_depth.Year <= end_year, :);
selected_evapotranspiration = evapotranspiration(evapotranspiration.Year >= start_year &...
    evapotranspiration.Year <= end_year, :);

selected_rainfall_depth = selected_rainfall_depth(:, 'Year', 'Month', 'Day', sub_basin);
selected_evapotranspiration = selected_evapotranspiration(:, 'Year', 'Month', 'Day', sub_basin);

% The store level should not start at zero, so a starting value is provided
% here.
sma_store_level = 5;

% As we now have the part of the timeseries selected that we want to look
% at, we can now determine the number of days that the model should run.
timesteps = length(selected_rainfall_depth.Day);

% The first step in the model is the determination of the net values for
% both inputs.
for timestep = 1:timesteps
    if rainfall_depth >= evapotranspiration
        net_rainfall_depth = rainfall_depth - evapotranspiration;
        net_evapotranspiration = 0;
        
        stored_rainfall = (sma_store_max_capacity * (1 - (sma_store_level/sma_store_max_capacity)^2) * ...
                tanh(net_rainfall_depth / sma_store_max_capacity))...
            / (1 + (sma_store_level/sma_store_max_capacity) * tanh(net_rainfall_depth / sma_store_max_capacity));
        store_evapotranspiration = 0;
        
    else
        net_rainfall_depth = 0;
        net_evapotranspiration = evapotranspiration - rainfall_depth;
        
        stored_rainfall = 0;
        store_evapotranspiration = (sma_store_level * (2 - sma_store_level/sma_store_max_capacity) * ...
            tanh(net_evapotranspiration / sma_store_max_capacity))...
            / (1 + (1 - sma_store_level/sma_store_max_capacity) * tanh(net_evapotranspiration / sma_store_max_capacity));
    end
    sma_store_level = sma_store_level - store_evapotranspiration + stored_rainfall;
    
    percolation = sma_store_level * (1 - (1 + (4/9 * sma_store_level/sma_store_max_capacity)^4)^(-1/4));
    
    sma_store_level = sma_store_level - percolation;
    
    routing_water = percolation + (net_rainfall_depth - stored_rainfall);
    
    if time_t <= 0
    end
    
   
     
    groundwater_exchange = exchange_coefficient * (routing_store_level / reference_capacity)^(7/2);
    routing_store_level = max([0; routing_store_level + UH1_output + groundwater_exchange]);
    
    outflow_reservoir = routing_store_level * (1 - (1 + (routing_store_level / reference_capacity)^4)^(-1/4));
    
    routing_store_level = routing_store_level - outflow_reservoir;
    
    outflow_direct = max([0; UH2_outflow + groundwater_exchange]);
    
    total_outflow = outflow_reservoir + outflow_direct;
end






