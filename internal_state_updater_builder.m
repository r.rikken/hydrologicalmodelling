addpath('classes')
addpath('results')

forecast_date = datetime('2020-01-06', 'InputFormat', 'yyyy-MM-dd');
loop_end_date = datetime('2020-01-20', 'InputFormat', 'yyyy-MM-dd');

calibration_results.Lesse = load('results/results_Lesse_1968_1982.mat');
calibration_results.Semois = load('results/results_Semois_1968_1982.mat');
calibration_results.Ourthe = load('results/results_Ourthe_1968_1982.mat');
calibration_results.Ambleve = load('results/results_Ourthe_1968_1982.mat');

index_basin.Semois = 1;
index_basin.Lesse = 3;
index_basin.Ourthe = 6;
index_basin.Ambleve = 7;

basins = ["Lesse", "Semois", "Ourthe", "Ambleve"];

for basin = basins
    index = 1;
    for forecast_index_date = forecast_date:loop_end_date
        forecast_index_date.Format = 'dd-MMM-yyyy';
        
        observed_values = load(strcat('data/', datestr(forecast_index_date), '/autres_rivieres.mat'));
        observed_value = str2double(table2array(observed_values.autres_rivieres(index_basin.(basin), 5)));
        internal_state = findClosestObservedValue(...
            observed_value, ...
            calibration_results.(basin)...
        );
        
        update_sets(index, 1).(basin) = Gr4jWarmup(...
            internal_state.production_store_level,...
            internal_state.routing_store_level,...
            internal_state.hydrograph_1,...
            internal_state.hydrograph_2...
        );
        
        index = index + 1;
    end
    
    save(strcat('data/update_sets'), 'update_sets')
end

function [model_state] = findClosestObservedValue(observed_value, result)

[closest_values, value_index] = min(abs((observed_value - result.results.total_outflow)));

model_state.total_outflow = result.results.total_outflow(value_index-1);
model_state.routing_store_level = result.results.routing_store_level(value_index-1);
model_state.production_store_level = result.results.sma_store_level(value_index-1);
model_state.hydrograph_1 = result.results.hydrograph_1_series(value_index-1);
model_state.hydrograph_2 = result.results.hydrograph_2_series(value_index-1);

end