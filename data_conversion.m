save('data/geographical_characteristics.mat', 'geographical_characteristics');

timeseries.discharge = observed_timeseries_1968_1982_discharge;
timeseries.potential_evapotranspiration = observed_timeseries_1968_1982_potential_evapotranspiration;
timeseries.precipitation = observed_timeseries_1968_1982_precipitation;
save('data/timeseries.mat', 'timeseries');