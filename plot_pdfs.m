x(1) = 209.1398;
x(2) = -1.1628;
x(3) = 64.0224;
x(4) = 2.3394;
x(5) = 0;

sigma(1) = 20;
sigma(2) = 0.2;
sigma(3) = 4;
sigma(4) = 0.3;
sigma(5) = 0.05;

tiledlayout(5,1)
% The number of times we draw a number for the Monte Carlo analysis.
number_of_draws = 10^6;
x_monte_carlo = zeros(4, number_of_draws);
for parameter_number = 1:5
    % The mu value for a normal distribution is the mean. In this case the
    % calibrated value for the parameter. The sigma is the standard
    % deviation. Chosen to get a good spread.
    normal_distribution = makedist('Normal', 'mu', x(parameter_number), 'sigma', sigma(parameter_number));
    random_from_uniform = rand(number_of_draws, 1);
    x_monte_carlo(parameter_number, :) = icdf(normal_distribution, random_from_uniform );
    
    x_for_plot = -sigma(parameter_number) * 3 + x(parameter_number) : .01 : sigma(parameter_number) * 3 + x(parameter_number);
    nexttile
    plot(x_for_plot, pdf(normal_distribution, x_for_plot))
end