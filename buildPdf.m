load('data/timeseries_1968_1982.mat');
time_series_1 = timeseries;
load('data/timeseries_1984_1998.mat');
time_series_2 = timeseries;
clear timeseries

sub_basin = 'Lesse';

precipitation = cat(1, ...
    table2array(time_series_1.precipitation(:, sub_basin)), ...
    table2array(time_series_2.precipitation(:, sub_basin)) ...
);
evapotranspiration = cat(1, ...
    table2array(time_series_1.potential_evapotranspiration(:, sub_basin)), ...
    table2array(time_series_2.potential_evapotranspiration(:, sub_basin)) ...
);

it_has_rained = precipitation > 0;
days_with_rain = sum(it_has_rained);
chance_of_rain = days_with_rain / length(precipitation);

evaporation_occurs = evapotranspiration > 0;
days_with_evaporation = sum(evaporation_occurs);
chance_of_evaporation = days_with_evaporation / length(evapotranspiration);

rainy_days = precipitation(precipitation > 0);
rain_distribution = fitdist(rainy_days, 'LogNormal');

evaporation_days = evapotranspiration(evapotranspiration > 0);
evaporation_distribution = fitdist(evaporation_days, 'LogNormal');

random_numbers = rand(length(evapotranspiration),1);
generated_precipitation = logninv(random_numbers, rain_distribution.mu, rain_distribution.sigma);
generated_precipitation = generated_precipitation .* (rand(length(evapotranspiration),1) < chance_of_rain);

random_numbers = rand(length(evapotranspiration),1);
generated_evaporation = logninv(random_numbers, evaporation_distribution.mu, evaporation_distribution.sigma);
generated_evaporation = generated_evaporation .* (rand(length(evapotranspiration), 1) < chance_of_evaporation);


