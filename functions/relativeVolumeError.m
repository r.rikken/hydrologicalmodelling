function [relative_volume_error] = relativeVolumeError(modelled_outflow, observed_outflow)
%RELATIVEVOLUMEERROR Relative volume error of modeled outflows
%   The relative volume error measure the volume error relative to the
%   modeled error.

relative_volume_error = sum(sum(modelled_outflow - observed_outflow)) /...
    sum(sum(modelled_outflow));

end

