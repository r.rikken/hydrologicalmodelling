function [hydrograph] = buildHydrograph2(hydrograph_time_base)
    total_timesteps = 2 * ceil(hydrograph_time_base) + 1;
    
    s_hydrograph = zeros(1,total_timesteps);
    hydrograph = zeros(1,total_timesteps);
    for time_index = 1:total_timesteps
        time_step = time_index - 1;
        if time_step <= 0
            s_hydrograph(time_index) = 0;
        elseif time_step > 0 && time_step <= hydrograph_time_base
            s_hydrograph(time_index) = (1/2) * (time_step/hydrograph_time_base).^(5/2);
        elseif time_step > hydrograph_time_base && time_step < 2 * hydrograph_time_base
            s_hydrograph(time_index) = 1 - (1/2) * (2 - time_step/hydrograph_time_base).^(5/2);
        else
            s_hydrograph(time_index) = 1;
        end
        
        if time_step > 0
            hydrograph(time_index - 1) = s_hydrograph(time_index) - s_hydrograph(time_index - 1);
        end
    end
    
    hydrograph = hydrograph(1:end-1);
end