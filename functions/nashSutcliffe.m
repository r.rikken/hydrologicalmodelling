function [efficiency_coefficient] = nashSutcliffe(modelled_discharges, observed_discharges)
%NASHSUTCLIFFE Nash-Sutcliffe model efficiency
% The Nash–Sutcliffe model efficiency coefficient (NSE) is used to assess the predictive power of hydrological models.  

mean_observed = mean(observed_discharges);
efficiency_coefficient = 1 - sum( (modelled_discharges - observed_discharges).^2 ) /...
                             sum( (observed_discharges - mean_observed).^2 );

end

