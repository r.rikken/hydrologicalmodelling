function [hydrograph] = buildHydrograph1(hydrograph_time_base)
    total_timesteps = ceil(hydrograph_time_base);
    timesteps = 0:total_timesteps;
    s_hydrograph = (timesteps/hydrograph_time_base).^(5/2);
    s_hydrograph(total_timesteps + 1) = 1;
    
    hydrograph = zeros(1,total_timesteps + 1);
    for time_step = 2:total_timesteps + 1
        hydrograph(time_step) = s_hydrograph(time_step) - s_hydrograph(time_step - 1);
    end
    
    hydrograph = hydrograph(2:end);
end