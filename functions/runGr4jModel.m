function [results, model_state] = runGr4jModel(precipitation, evapotranspiration, parameters, warm_up)
%RUNGR4JMODEL GR4J Model
%   The GR4J (modèle du Génie Rural à 4 paramètres Journalier) model is a soil moisture accounting model.
% Model input of the GR4J is the rainfall depth and the potential
% evapotranspiration

% Maximum capacity of the production store, x_1 in literature.
% Can only be a positive number
sma_store_max_capacity = parameters.getX1();
% Water exchange coefficient for the routing store, x_2 in literature
exchange_coefficient = parameters.getX2();
% Routing store maximum capacity, x_3 in literature
% Can only be a positive number
reference_capacity = parameters.getX3();
% Hydrograph time parameter, named x_4 in literature.
% Can only be greater than 0.5
hydrograph_time_base = parameters.getX4();

% When the values for the warm up are passed, set them into the model
% variables.
if exist('warm_up', 'var')
    sma_store_level = warm_up.getProductionStoreLevel();
    routing_store_level = warm_up.getRoutingStoreLevel();
else
    sma_store_level = 0;
    routing_store_level = 0;
end

% As we now have the part of the timeseries selected that we want to look
% at, we can now determine the number of days that the model should run.
timesteps = length(precipitation);

% To delay the outflow of the percipitation, two unit hydrographs are
% used. One for the direct flow, and one for the flow to the routing
% reservoir. Because the unit hydrographs do not change in time, we
% calculate them here.
unit_hydrograph_1 = buildHydrograph1(hydrograph_time_base);
unit_hydrograph_2 = buildHydrograph2(hydrograph_time_base);

% We need the lengths to be able to index into the hydrograph series and we
% preallocate the series here. These series need to be longer than the
% maximum timesteps to account for the adding of the last few steps in a
% timeseries.
hydrograph_1_length = length(unit_hydrograph_1);
hydrograph_2_length = length(unit_hydrograph_2);
hydrograph_1_series = zeros(1, timesteps + hydrograph_1_length - 1);
hydrograph_2_series = zeros(1, timesteps + hydrograph_2_length - 1);

% If there is a warmup for the hydrographs specified, add these to the
% hydrograph series with the correct time base length.
hydrograph_1_updater = zeros(1, ceil(hydrograph_time_base));
if any(warm_up.getRoutingHydrographState() ~= 0)
    hydrograph_1_updater = warm_up.getRoutingHydrographState();
    hydrograph_1_series(1, 1:ceil(hydrograph_time_base)) = ...
        hydrograph_1_updater(1, 1:ceil(hydrograph_time_base));
end
hydrograph_2_updater = zeros(1, 2*ceil(hydrograph_time_base));
if any(warm_up.getDirectHydrographState() ~= 0)
    hydrograph_2_updater = warm_up.getDirectHydrographState();
    hydrograph_2_series(1, 1:2*ceil(hydrograph_time_base)) = ...
        hydrograph_2_updater(1, 1:2*ceil(hydrograph_time_base));
end

% The first step in the model is the determination of the net values for
% both inputs.
for timestep = 1:timesteps
    if precipitation(timestep) >= evapotranspiration(timestep)
        net_rainfall_depth = precipitation(timestep) - evapotranspiration(timestep);
        rainfall_for_storage = (sma_store_max_capacity * (1 - (sma_store_level/sma_store_max_capacity)^2) * ...
                tanh(net_rainfall_depth / sma_store_max_capacity))...
            / (1 + (sma_store_level/sma_store_max_capacity) * tanh(net_rainfall_depth / sma_store_max_capacity));
        evapotranspiration_from_storage = 0;
        
    else
        net_rainfall_depth = 0;
        net_evapotranspiration = evapotranspiration(timestep) - precipitation(timestep);
        
        rainfall_for_storage = 0;
        evapotranspiration_from_storage = (sma_store_level * (2 - sma_store_level/sma_store_max_capacity) * ...
            tanh(net_evapotranspiration / sma_store_max_capacity))...
            / (1 + (1 - sma_store_level/sma_store_max_capacity) * tanh(net_evapotranspiration / sma_store_max_capacity));
    end
    sma_store_level_new = sma_store_level - evapotranspiration_from_storage + rainfall_for_storage;
    
    percolation = sma_store_level_new * (1 - (1 + (4/9 * sma_store_level/sma_store_max_capacity)^4)^(-1/4));
    
    sma_store_level_newest = sma_store_level_new - percolation;
    
    % We now calculate the water available for routing. Ten percent will
    % directly flow out of the basin, ninety percent will go to the
    % storage.
    routing_water = percolation + (net_rainfall_depth - rainfall_for_storage);
    routing_water_10 = routing_water * 0.1;
    routing_water_90 = routing_water * 0.9;
    
    % The unit hydrograph can now multiplied by the routing water, and so
    % become proper hydrographs.
    hydrograph_1_series(1, timestep:timestep + hydrograph_1_length - 1) =...
        hydrograph_1_series(1, timestep:timestep + hydrograph_1_length - 1) + unit_hydrograph_1 * routing_water_90;
    hydrograph_2_series(1, timestep:timestep + hydrograph_2_length - 1) =...
        hydrograph_2_series(1, timestep:timestep + hydrograph_2_length - 1) + unit_hydrograph_2 * routing_water_10;
    
    groundwater_exchange = exchange_coefficient * (routing_store_level / reference_capacity)^(7/2);
    routing_store_level_new = max([0; routing_store_level + hydrograph_1_series(timestep) + groundwater_exchange]);
    
    outflow_reservoir = routing_store_level_new * (1 - (1 + (routing_store_level_new / reference_capacity)^4)^(-1/4));
    
    routing_store_level_newest = routing_store_level_new - outflow_reservoir;
    
    outflow_direct = max([0; hydrograph_2_series(timestep) + groundwater_exchange]);
    
    results.total_outflow(timestep) = outflow_reservoir + outflow_direct;
    
    sma_store_level = sma_store_level_newest;
    routing_store_level = routing_store_level_newest;
    
    results.routing_store_level(timestep) = routing_store_level;
    results.sma_store_level(timestep) = sma_store_level;
end

results.hydrograph_1_series = hydrograph_1_series;
results.hydrograph_2_series = hydrograph_2_series;

model_state.production_store_level = sma_store_level;
model_state.routing_store_level = routing_store_level;
model_state.routing_hydrograph_state = hydrograph_1_series(1:ceil(hydrograph_time_base))  - hydrograph_1_updater;
model_state.direct_hydrograph_state = hydrograph_2_series(1:2 * ceil(hydrograph_time_base)) - hydrograph_2_updater;

end