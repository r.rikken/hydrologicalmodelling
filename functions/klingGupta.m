function [kling_gupta_efficiency] = klingGupta(modelled_outflow, observed_outflow)
%KLINGGUPTA Kling Gupta efficiency metric
%   This goodness-of-fit measure was developed by Gupta et al. (2009) to provide a diagnostically interesting
%   decomposition of the Nash-Sutcliffe efficiency (and hence MSE), which facilitates the analysis of the relative
%   importance of its different components (correlation, bias and variability) in the context of hydrological modelling
%   Kling et al. (2012), proposed a revised version of this index, to ensure that the bias and variability ratios are
%   not cross-correlated

modelled_s_d = std(modelled_outflow);
observed_s_d = std(observed_outflow);
modelled_mean = mean(modelled_outflow);
observed_mean = mean(observed_outflow);

enumerator = sum((observed_outflow - observed_mean) .* (modelled_outflow - modelled_mean));
denominator = sqrt(sum((observed_outflow - observed_mean).^2)) * sqrt(sum((modelled_outflow - modelled_mean).^2));
pearson_coefficient =  enumerator / denominator;

kling_gupta_efficiency = 1 - sqrt((1 - pearson_coefficient)^2 +...
    (1 - modelled_s_d/observed_s_d)^2 + (1 - modelled_mean/observed_mean)^2);
end

