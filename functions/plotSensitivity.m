function [] = plotSensitivity(kling_gupta)
%PLOTSENSITIVITY Summary of this function goes here
%   Detailed explanation goes here

    % Maximum capacity of the production store, x_1 in literature.
    % Water exchange coefficient for the routing store, x_2 in literature
    % Routing store maximum capacity, x_3 in literature
    % Hydrograph time parameter, named x_4 in literature.
    % x_3, X_1, x_2, x_4
    
production_store(:) = kling_gupta(10, :, 10, 10);
exchange_coefficient(:) = kling_gupta(10, 10, :, 10);
routing_store_max(:) = kling_gupta(:, 10, 10, 10);
hydrograph_time(:) = kling_gupta(10, 10, 10, :);

YMatrix1(:,1) = production_store;
YMatrix1(:,2) = exchange_coefficient;
YMatrix1(:,3) = routing_store_max;
YMatrix1(:,4) = hydrograph_time;

% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');

% Create multiple lines using matrix input to plot
plot1 = plot(YMatrix1,'LineWidth',2,'Parent',axes1);
set(plot1(1),'DisplayName','Production store (x1)');
set(plot1(2),'DisplayName','Exchange Coefficient (x2)');
set(plot1(3),'DisplayName','Routing Store (x3)');
set(plot1(4),'DisplayName','hydrogtraph time (x4)');

% Create ylabel
ylabel({'Kling Gupta Efficiency'});

% Create xlabel
xlabel({'Percentage change'});

% Create title
title({'Parameter Sensitivity'});

xticks([1 3 5 7 9 11 13 15 17 19 20])
xticklabels({'10', '30', '50', '70', '90', '110', '130', '150', '170', '190'})

box(axes1,'on');
% Create legend
legend1 = legend(axes1,'show');
set(legend1,'Location','southeast','FontSize',12);
end

