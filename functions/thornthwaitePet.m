function [potential_evapotranspiration] = thornthwaitePet(average_daily_temperature, number_of_days, day_length)
%THORNTHWAITEPET The potential evapotranspiration as calculated by
%Thornthwaite (1948)
%
% average_daily_temperature in degrees Celsius (must be positive)
% number_of_days
% day_length in number of sunlight hours averaged over the month
% average_monthly_temperature in degrees Celsius
average_monthly_temperature = [3 6 9 13 16 19 20 20 18 15 7 4];
heat_index = sum( (average_monthly_temperature ./ 5).^(1.514) );
alpha = (6.75 * 10^(-7)) * heat_index^3 ...
    - (7.71 * 10^(-5)) * heat_index^2 ...
    + (1.792 * 10^(-2) * heat_index ...
    + 0.49239);

if average_daily_temperature < 0
    average_daily_temperature = 0;
end

potential_evapotranspiration =  16 * (day_length / 12) ...
    * (number_of_days / 30 ) ...
    * (10 * average_daily_temperature/heat_index)^alpha;

end