classdef Gr4jParameters
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    %
    %       X1              X2                  X3            X4
    % Min.   :  126     Min.    :-54.5   Min.    :  8   Min.   :1.21
    % 1st Qu.:  208     1st Qu. : -2.0   1st Qu. : 35   1st Qu.:1.75
    % Median :  291     Median  : -1.1   Median  : 76   Median :2.10
    % Mean   :  471     Mean    : -3.4   Mean    : 90   Mean   :2.09
    % 3rd Qu.:  359     3rd Qu. : -0.6   3rd Qu. :106   3rd Qu.:2.45
    % Max.   : 4006     Max.    :  0.8   Max.    :318   Max.   :3.47
    %
    % Lesse: 209.1398       -1.1628        64.0224         2.3394
    % Semois : 200            -1.1           95              3.25
    % Ourthe : 175            -1.2           70              2.5
    % Ambleve: 220.5          -0.52          104.5           2
    % Vesdre : 285            -1.2           63              1.815
    
    
    properties
        production_store_capacity = null(1);
        exchange_coefficient = null(1);
        routing_store_capacity = null(1);
        hydrograph_time_base = null(1);
    end
    
    methods
        function obj = Gr4jParameters(x_1, x_2, x_3, x_4)
            obj.production_store_capacity = x_1;
            obj.exchange_coefficient = x_2;
            obj.routing_store_capacity = x_3;
            obj.hydrograph_time_base = x_4;
        end
        
        function capacity = getProductionStoreCapacity(obj)
            capacity = obj.production_store_capacity;
        end
        
        function capacity = getX1(obj)
            capacity = obj.production_store_capacity;
        end
        
        function value = getExchangeCoefficient(obj)
            value = obj.exchange_coefficient;
        end
        
        function value = getX2(obj)
            value = obj.exchange_coefficient;
        end
        
        function capacity = getReferenceCapacity(obj)
            capacity = obj.routing_store_capacity;
        end
        
        function capacity = getX3(obj)
            capacity = obj.routing_store_capacity;
        end
        
        function time = getHydrographTimeBase(obj)
            time = obj.hydrograph_time_base;
        end
        
        function time = getX4(obj)
            time = obj.hydrograph_time_base;
        end
    end
end
