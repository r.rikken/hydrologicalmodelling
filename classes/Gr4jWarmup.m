classdef Gr4jWarmup
    %GR4JWARMUP Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        production_store_level = 0;
        routing_store_level = 0; 
        direct_hydrograph_state = 0;
        routing_hydrograph_state = 0;
    end
    
    methods
        function obj = Gr4jWarmup(...
                production_store_level,...
                routing_store_level,...
                direct_hydrograph_state,...
                routing_hydrograph_state...
            )
            %GR4JWARMUP Construct an instance of this class
            %   Detailed explanation goes here
            obj.production_store_level = production_store_level;
            obj.routing_store_level = routing_store_level;
            if exist('direct_hydrograph_state', 'var')
                obj.direct_hydrograph_state = direct_hydrograph_state;
            end
            if exist('routing_hydrograph_state', 'var')
                obj.routing_hydrograph_state = routing_hydrograph_state;
            end
        end
        
        function store_level = getProductionStoreLevel(obj)
            store_level = obj.production_store_level;
        end
        
        function obj = setProductionStoreLevel(obj, store_level)
            obj.production_store_level = store_level;
        end
        
        function store_level = getRoutingStoreLevel(obj)
            store_level = obj.routing_store_level;
        end
        
        function obj = setRoutingStoreLevel(obj, store_level)
            obj.production_store_level = store_level;
        end
        
         function hydrograph_state = getDirectHydrographState(obj)
            hydrograph_state = obj.direct_hydrograph_state;
        end
        
        function obj = setDirectHydrographState(obj, hydrograph_state)
            obj.direct_hydrograph_state = hydrograph_state;
        end
        
        function hydrograph_state = getRoutingHydrographState(obj)
            hydrograph_state = obj.routing_hydrograph_state;
        end
        
        function obj = setRoutingHydrographState(obj, hydrograph_state)
            obj.routing_hydrograph_state = hydrograph_state;
        end
    end
end
