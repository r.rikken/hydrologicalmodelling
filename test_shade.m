plot_outflow = zeros(100000, 5479);
for index = 1:100000
    plot_outflow(index, :) = results(index).total_outflow';
end

start_year = 1964;
end_year = 1982;
sub_basin = 'Lesse';

selected_measured_discharge = timeseries.discharge(timeseries.discharge.Year...
    >= start_year & timeseries.discharge.Year <= end_year, :);
measured_discharge = table2array(selected_measured_discharge(:, sub_basin));

plot_line_max = max(plot_outflow, [], 1)';
plot_line_min = min(plot_outflow, [], 1)';
plot_line_max(plot_line_max < 0.0001) = 0.0001;
x = 1:5479;

% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');

X = [x fliplr(x)];
Y = [plot_line_min' fliplr(plot_line_max')];

patch('Parent',axes1,'YData',Y,'XData',X,'FaceColor',[0.8 0.8 0.8], ...
    'EdgeColor',[0.8 0.8 0.8])
plot1 = plot(x, measured_discharge, 'LineWidth', 1, 'Color', [0 0 1]);
plot2 = plot(x, results_.total_outflow, 'LineWidth', 1, 'Color', [0 0.5 0.5]);
plot3 = plot(x, ones(1, length(x)) .* mean(mean(plot_outflow)), 'LineWidth', 1, 'Color', [0.5 0 0]);
plot4 = plot(x, ones(1, length(x)) .* 2.1835, 'LineWidth', 1, 'Color', [0.5 0.5 0]);

start_year = 1964;
end_year = 1982;
sub_basin = 'Lesse';

selected_evapotranspiration = timeseries.potential_evapotranspiration(timeseries.potential_evapotranspiration.Year...
    >= start_year & timeseries.potential_evapotranspiration.Year <= end_year, :);
base_evapotranspiration = table2array(selected_evapotranspiration(:, sub_basin));

plot_line_max = max(new_evapotranspiration, [], 2)';
plot_line_min = min(new_evapotranspiration, [], 2)';
plot_line_max(plot_line_max < 0.0001) = 0.0001;
x = 1:5479;

% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');

x2 = [x, fliplr(x)];
inbetween = [plot_line_max, fliplr(plot_line_min)];

patch('Parent',axes1,'YData',inbetween,'XData',x2,'FaceColor',[0.8 0.8 0.8], ...
    'EdgeColor',[0.8 0.8 0.8])
plot1 = plot(x, base_evapotranspiration,'LineWidth',1,'Color',[0 0 0]);

plot(x, plot_line_max, 'k', 'LineWidth', 1);
hold on;
plot(x, plot_line_min, 'k', 'LineWidth', 1);
x2 = [x, fliplr(x)];
inbetween = [plot_line_max, fliplr(plot_line_min)];
fill(x2, inbetween , [0.8 0.8 0.8]);

x = 1 : 300;
curve1 = log(x);
curve2 = 2*log(x);
plot(x, curve1, 'r', 'LineWidth', 2);
hold on;
plot(x, curve2, 'b', 'LineWidth', 2);
x2 = [x, fliplr(x)];
inBetween = [curve1, fliplr(curve2)];
fill(x2, inBetween, 'g');