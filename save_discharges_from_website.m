%{

This script saves the information from the Wallonie webpage.
It uses the Text Analytics Toolbox, so that first needs to be installed
before using this script.

In the whole script, no preallocation is done, because the perfomance is
not an issue, so you can ignore the warnings :p .
There may be warnings if the directories for the dates and data already
exists. If you want to remove them, a try and catch block is probably
needed.

%}

% Make a directory named data if it does not already exists.
mkdir('data')

% This is the url for the measured discharge and rainfall in the Wallonie
% area.
url = "http://voies-hydrauliques.wallonie.be/opencms/opencms/fr/hydro/Actuelle/bulhyd.html";

% Read the html page and put it in a htmlTree format, so we can navigate
% the html tree.
html_page = htmlTree(webread(url));

% The findElement uses the normal css selectors for finding the correct
% html element.
retrait_tables = findElement(html_page, 'DIV.retrait > TABLE');

% We want to save every table on the page, so loop through them all.
for table_index = 1:length(retrait_tables)
    % To give every table column a name, we filter the headers from the
    % table.
    table_headers = findElement(retrait_tables(table_index), 'TH');
    
    % Get the names of the table headers and use them to name the
    % variables.
    for header_index = 1:length(table_headers)
        variable_string = extractHTMLText(table_headers(header_index));
        
        % Clean up the header names, so they are easier to use.
        variable_string = erase(variable_string, 'Date : ');
        variable_names(header_index, 1) = replace(variable_string, 'è', 'e');
    end
    
    % Now that we have the table header names, we want to get the data in
    % the table.
    table_rows = findElement(retrait_tables(table_index), 'TR');
    
    % We skip the first row, because they contain the table headers.
    for row_index = 2:length(table_rows)
        table_row = table_rows(row_index);
        % The values of the table are return as a column, so transpose the
        % returned vector. 
        % TODO: The values are all in strings, so maybe add a conversion
        % to double here or later when the data is in table format.
        table_values = extractHTMLText(findElement(table_row, 'TD'))';
        
        % Add the values to a new table row. This is minus one, because we
        % skipped the header row earlier.
        data_table(row_index - 1, :) = table_values;
    end
    
    % Make a directory to save the data of today, if it does not already
    % exists.
    mkdir(strcat('data/', date));
    
    % We now save the three tables under three different names.
    switch table_index
        case 1
            voies_navigables = array2table(data_table, 'VariableNames', variable_names);
            save(strcat('data/', date, '/voies_navigables.mat'), 'voies_navigables')
        case 2
            autres_rivieres = array2table(data_table, 'VariableNames', variable_names);
            save(strcat('data/', date, '/autres_rivieres.mat'), 'autres_rivieres')
        case 3
            pluies = array2table(data_table, 'VariableNames', variable_names);
            save(strcat('data/', date, '/pluies.mat'), 'pluies')
    end
end