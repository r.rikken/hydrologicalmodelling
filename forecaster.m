addpath(genpath('data'))
addpath(genpath('functions'))
addpath(genpath('classes'))

load('data/geographical_characteristics.mat')

basins = ["Lesse", "Semois", "Ourthe"];

use_internal_state_updater = true;
use_mean_updater = false;

index_basin.Semois = 1;
index_basin.Lesse = 3;
index_basin.Ourthe = 6;
index_basin.Ambleve = 7;

parameter_sets.Lesse   = Gr4jParameters(209.14,  -1.16,  64,     2.34  );
parameter_sets.Semois  = Gr4jParameters(200,     -1.1,   95,     3.25  );
parameter_sets.Ourthe  = Gr4jParameters(175,     -1.2,   70,     2.5   );
parameter_sets.Vesdre  = Gr4jParameters(285,     -1.2,   63,     1.815 );

warm_up_sets.Lesse  = Gr4jWarmup(188, 46);
warm_up_sets.Semois = Gr4jWarmup(180, 65);
warm_up_sets.Ourthe = Gr4jWarmup(180, 45);
warm_up_sets.Vesdre = Gr4jWarmup(210, 45);

observations_start_date = datetime('2020-01-06', 'InputFormat', 'yyyy-MM-dd');
data_start_date = datetime('2019-12-20', 'InputFormat', 'yyyy-MM-dd');
data_end_date = datetime('2020-01-20', 'InputFormat', 'yyyy-MM-dd');

update_sets = load('data/update_sets');

% Run the GR4J model for the basins
for basin = basins
    basin_area = geographical_characteristics(geographical_characteristics.Basin == basin, :).Size;
    warm_up_set = warm_up_sets.(basin);
    update_set_index = 1;
    for date_index = data_start_date:data_end_date
        if date_index >= observations_start_date + days(1) && use_internal_state_updater
            update_set = update_sets.update_sets(update_set_index).(basin);
            update_set_index = update_set_index + 1;
            
            hydrograph_update_value = update_set.getRoutingHydrographState();
            update_with = hydrograph_update_value - model_state.routing_hydrograph_state(1);
            updated_routing_hydrograph = model_state.routing_hydrograph_state + update_with;
            updated_routing_hydrograph(updated_routing_hydrograph < 0) = 0;
            
            hydrograph_update_value = update_set.getDirectHydrographState();
            update_with = hydrograph_update_value - model_state.direct_hydrograph_state(1);
            updated_direct_hydrograph = model_state.direct_hydrograph_state + update_with;
            updated_direct_hydrograph(updated_direct_hydrograph < 0) = 0;
            
            time_base = parameter_sets.(basin).getX4();
            
            warm_up_set = Gr4jWarmup(...
                update_set.getProductionStoreLevel(),...
                update_set.getRoutingStoreLevel(),...
                updated_direct_hydrograph,...
                updated_routing_hydrograph...
                );
        end
        
        precipitation = buildPrecipitationArray(date_index, basin);
        evapotranspiration = buildEvapotranspirationArray(date_index, basin);
        
        [results, model_state] = runGr4jModel(...
            precipitation,...
            evapotranspiration,...
            parameter_sets.(basin),...
            warm_up_set...
            );
        
        results.total_outflow = results.total_outflow ./ 1000 ./ 86400 .* basin_area .* 10^6 * 0.7;
        
        if date_index >= observations_start_date && use_internal_state_updater
            file_name = strcat('data/', datestr(date_index), '/results_internal_updater_', basin, '.mat');
        else
            file_name = strcat('data/', datestr(date_index), '/results_', basin, '.mat');
        end
        mkdir(strcat('data/', datestr(date_index)));
        date_index.Format = 'dd-MMM-yyyy';
        save(file_name, 'results');
    end
    
    if use_mean_updater == true
        for date_index = observations_start_date + days(1):data_end_date
            observed_values = load(strcat('data/', datestr(date_index - days(1)), '/autres_rivieres.mat'));
            observed_values_two = load(strcat('data/', datestr(date_index), '/autres_rivieres.mat'));
            observed_value = mean([
                str2double(table2array(observed_values.autres_rivieres(index_basin.(basin), 5)))
                str2double(table2array(observed_values_two.autres_rivieres(index_basin.(basin), 5)))
                ]);
            
            results = load(strcat('data/', datestr(date_index), '/results_', basin, '.mat'));
            difference = observed_value - results.results.total_outflow(1);
            corrected_results = results.results.total_outflow + difference;
            
            file_name = strcat('data/', datestr(date_index), '/results_mean_updater_', basin, '.mat');
            mkdir(strcat('data/', datestr(date_index)));
            date_index.Format = 'dd-MMM-yyyy';
            save(file_name, 'corrected_results');
        end
    end
end

%% Functions to get the data in the correct format
function precipitation = buildPrecipitationArray(date, basin_name)

directory = '/data/forecasts/';
day = sprintf('%02d', date.Day);
month = sprintf('%02d', date.Month);
year = sprintf('%02d', date.Year);

name = strcat('Meuse_Catchments_GFS_Precipitation.', year, month, day, '0400');
ftp_precipitation = importPrecipitation(strcat(directory, name));

precipitation = zeros(10, 1);
for day_index = 1:10
    precipitation_on_day = ftp_precipitation(...
        ftp_precipitation.Time > date + days(day_index - 1) ...
        & ftp_precipitation.Time < date + days(day_index), :);
    precipitation(day_index) = sum(precipitation_on_day.(basin_name));
end
end

function evapotranspiration = buildEvapotranspirationArray(date, basin_name)

directory = '/data/forecasts/';
day = sprintf('%02d', date.Day);
month = sprintf('%02d', date.Month);
year = sprintf('%02d', date.Year);

name = strcat('Meuse_Catchments_GFS_Temperature.', year, month, day, '0400');
ftp_temperature = importEvapotranspiration(strcat(directory, name));

evapotranspiration = zeros(10, 1);
for day_index = 1:10
    temperature_on_day = ftp_temperature(...
        ftp_temperature.Time > date + days(day_index - 1) ...
        & ftp_temperature.Time < date + days(day_index), :);
    mean_temperature = mean(temperature_on_day.(basin_name));
    
    evapotranspiration(day_index) = thornthwaitePet(mean_temperature, 1, 3);
end
end
